rails g controller Home index about contact faq policy
rails g devise:install
rails g devise User
rails g devise:views
foreman run rake db:migrate
rails g scaffold Challenge name:string description:text moderation_flag:boolean status:string terms:text creator:references{polymorphic} requirement:text deadline:datetime payoff:integer open_flag:boolean min_num:integer max_num:integer
rails g scaffold Group name:string description:text
rails g model UserGroup user:references group:references
rails g model Category name:string
rails g model ChallengeCategory challenge:references category:references
rails g scaffold Comment user:references challenge:references comment:text
rails g model Accept acceptor:references{polymorphic} accepted:references{polymorphic}
rails g model Contribution contributor:references{polymorphic} contributed:references{polymorphic} amount:integer charity:string
rails g migration AddDetailsToUser first_name:string last_name:string date_of_birth:date phone_number:string admin_flag:boolean
rails g controller charges
