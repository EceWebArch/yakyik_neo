Rails.application.routes.draw do

  root                          'home#index'
  get 'about'               =>  'home#about'
  get 'contact'             =>  'home#contact'
  get 'faq'                 =>  'home#faq'
  get 'policy'              =>  'home#policy'

  devise_for :users

resources :challenges do
  resources :comments
end

  resources :comments
  resources :groups
  resources :challenges
  resources :charges

end
