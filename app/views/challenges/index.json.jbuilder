json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :name, :description, :moderation_flag, :status, :terms, :creator_id, :creator_type, :requirement, :deadline, :payoff, :open_flag, :min_num, :max_num
  json.url challenge_url(challenge, format: :json)
end
