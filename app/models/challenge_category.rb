class ChallengeCategory < ActiveRecord::Base

  belongs_to :challenge
  belongs_to :category
  
end
