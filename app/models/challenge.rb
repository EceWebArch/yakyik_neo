class Challenge < ActiveRecord::Base

    belongs_to :creator, polymorphic: true

    has_many :accepts, as: :accepted
    has_many :user_acceptors, :through=> :accepts, :source => :acceptor, source_type: "User"
    has_many :group_acceptors, :through=> :accepts, :source => :acceptor, source_type: "Group"
    has_many :comments
    has_many :user_contributors, :through=> :contributions, :source => :contributor, source_type: "User"
    has_many :group_contributors, :through=> :contributions, :source => :contributor, source_type: "Group"
    has_many :contributions, as: :contributed

    has_and_belongs_to_many :categories

    validates :name,            presence: :true,
                                length: { maximum: 45 }
    validates :description,     presence: :true,
                                length: { within: 20..250 }

end
