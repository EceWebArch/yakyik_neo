class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :accepts, as: :acceptor
  has_many :accepted, :through => :accepts, :source => :accepted, source_type: "Challenge"
  has_many :comments
  has_many :challenges, as: :creator
  has_many :contributed, :through => :contributions, :source => :contributed, source_type: "Challenge"
  has_many :contributions, as: :contributor
  has_many :groups, through: :user_groups
  has_many :user_groups

has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100#" }
validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
