class CreateChallengeCategories < ActiveRecord::Migration
  def change
    create_table :challenge_categories do |t|
      t.references :challenge, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
